export const prerender = false;

import type { APIRoute } from "astro";
import { MailerSend, EmailParams, Sender, Recipient } from "mailersend";

export const POST: APIRoute = async ({ request }) => {
  // Get data from POST request
  const data = await request.formData();
  const name = data.get("name");
  const phone = data.get("phone");
  const email = data.get("email");
  const message = data.get("message");

  // Validate the data - you'll probably want to do more than this
  if (!name || !email || !message || !phone) {
    return new Response(
      JSON.stringify({
        message: "Missing required fields",
      }),
      { status: 400 }
    );
  }

  // Set up MailerSend instance
  const api_key = import.meta.env.MAILERSEND_API_KEY;
  const mailersend = new MailerSend({ apiKey: api_key });

  const recipients = [new Recipient('marc@duby.zone', 'Marc Duby')];
  const sentFrom = new Sender("eightdayrain@pixelpoetry.dev", "Eight Day Rain");

  const variables = [
    {
      email: 'marc@duby.zone',
      substitutions: [
        { var: 'name', value: name.toString() },
        { var: 'email', value: email.toString() },
        { var: 'phone', value: phone.toString() },
        { var: 'message', value: message.toString() },
      ],
    },
  ];

  const emailParams = new EmailParams()
    .setFrom(sentFrom)
    .setTo(recipients)
    .setReplyTo(sentFrom)
    .setSubject('New Contact Form Submission')
    .setTemplateId('0r83ql3ny9m4zw1j') // Ensure you use the correct template ID here
    .setVariables(variables);

  try {
    await mailersend.email.send(emailParams);
    return new Response(
      JSON.stringify({ message: "Thanks for your message! We'll get back to you as soon as possible." }),
      { status: 200, headers: { 'Content-Type': 'application/json' } }
    );
  } catch (error) {
    console.error("Error sending email:", error);
    return new Response(
      JSON.stringify({ message: 'Failed to send email', error: error }),
      { status: 500, headers: { 'Content-Type': 'application/json' } }
    );
  }
};