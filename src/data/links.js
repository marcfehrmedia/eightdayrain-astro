export const NAV_LINKS = [
  {
    id: 'home',
    text: 'Home',
    href: '/'
  },
  {
    id: 'about',
    text: 'About',
    href: '/about'
  },
  {
    id: 'contact',
    text: 'Contact',
    href: '/contact'
  },
]