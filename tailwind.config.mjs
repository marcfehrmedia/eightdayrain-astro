import defaultTheme from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */

export default {
  darkMode: 'media',
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      colors: {
        'nightsky': { '50': '#ebefff', '100': '#dbe1ff', '200': '#bdc7ff', '300': '#96a2ff', '400': '#6c70ff', '500': '#534aff', '600': '#452aff', '700': '#3b1fe3', '800': '#301cb7', '900': '#2c208f', '950': '#160f43', },
        'sandstone': { '50': '#fdf8ed', '100': '#f8eacd', '200': '#f1d296', '300': '#eab65f', '400': '#e59c3a', '500': '#de8028', '600': '#c45d1b', '700': '#a3411a', '800': '#85341b', '900': '#6d2c1a', '950': '#3e140a', },
        'greyfog': { '50': '#f5f6f8', '100': '#ededf2', '200': '#dedfe7', '300': '#c9cad8', '400': '#b4b3c6', '500': '#aba8bd', '600': '#8e88a3', '700': '#7b758d', '800': '#646073', '900': '#53515e', '950': '#312f37', },
      },
      fontFamily: {
        sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
        serif: ['Alegreya', ...defaultTheme.fontFamily.serif],
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@rvxlab/tailwind-plugin-ios-full-height')
  ],
}
